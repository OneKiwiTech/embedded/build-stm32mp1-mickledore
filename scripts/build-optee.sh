#!/bin/bash

clone_atf() {
    if [ ! -d "atf-stm32mp" ]; then
        git clone git@github.com:OneKiwiPublic/optee_os-stm32mp.git -b onekiwi-3.19.0-stm32mp-r1
    fi
}

build_atf() {
    cd optee_os-stm32mp
    unset -v CFLAGS LDFLAGS 
    make PLATFORM=stm32mp1 CFG_EMBED_DTB_SOURCE_FILE=${DEVICE_NAME}.dts CFG_TEE_CORE_LOG_LEVEL=2 CFLAGS32=--sysroot=${SDKTARGETSYSROOT} O=build all
}

source ./scripts/build-sdk.sh

clone_atf
build_atf