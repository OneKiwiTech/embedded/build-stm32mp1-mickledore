#!/bin/bash

clone_linux() {
    if [ ! -d "linux-stm32mp" ]; then
        git clone git@github.com:OneKiwiPublic/linux-stm32mp.git -b onekiwi-v6.1-stm32mp-r1
    fi
}

build_linux() {
    cd linux-stm32mp
    export KBUILD_OUTPUT=./build
    make multi_v7_defconfig
    make ${DEVICE_NAME}.dtb -j8
    make ${DEVICE_NAME}-a7-examples.dtb -j8
    make ${DEVICE_NAME}-m4-examples.dtb -j8
}

source ./scripts/build-sdk.sh

clone_linux
build_linux